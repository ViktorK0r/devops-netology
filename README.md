# devops-netology

Файл terraform/.gitignore одержит информацию для игнорирования:

1) **/.terraform/*

Локальных директорий terraform в любом каталоге/подкаталоге


2) *.tfstate *.tfstate.*

Файлов с расширением .tfstate/.tfstate.*


3) crash.log

Файлов логирования crash.log


4) *.tfvars

Файлов с расширением .tfvars которые могут иметь конфиденциальные данные (пароли, приватные ключи и пр.)


5) override.tf override.tf.json *_override.tf *_override.tf.json

Файлов перераспределения локальных ресурсов, поэтому нет смысла распространять


6) .terraformrc terraform.rc

Файлов конфигурации CLI

